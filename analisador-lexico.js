let fs = require('fs'); // Importação da biblioteca file system para leitura e escrita de arquivos.
var readline = require('readline'); // Importação da biblioteca readline para capturar a escrita do usuario
var leitor = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const palavrasReservadas = ['programa', 'begin', 'end', 'if', 'else', 'then', 'do', 'while', 'until', 'repeat', 'integer', 'real', 'string', 'all', 'and', 'or'];
const simples = ['.', ',', ';', '(', ')', '=', '+', '-', '*', '/'];
const composto = [':', '<', '>'];  // ':=', '<>', '>=', '<=', '>', '<'
const letras = 'abcdefghijlmnopqrstuvxzywk';
const numeros = '1234567890';
const identificadores = letras + numeros;

var linha = 1;
var coluna = 0;
var letra = -1;
var caractere = 0;
var tabelalex = []; // linha, coluna, tipo, valor 
var tabelaLexica = '';
var codigo = '';


function digerir() {
    letra++;
    coluna++;
    caractere = letra < codigo.length ? codigo[letra] : String.fromCharCode(0);
    if (caractere == '\n') {
        linha++;
        coluna = 0;
    }
    return caractere;
}

function lerArquivo() {
    leitor.question('Nome do arquivo: ', (arquivo) => {
        // arquivo = 'teste.txt';
        fs.readFile(arquivo, 'utf8', (error, data) => {
            if (data) {
                codigo = data.toLowerCase();

                digerir();
                while (caractere !== String.fromCharCode(0)) {

                    if (caractere == '{') {
                        while (digerir() != '}' && caractere !== String.fromCharCode(0));

                    } else if (simples.indexOf(caractere) > -1) {
                        tabelalex.push([linha, coluna, caractere, caractere]);

                    } else if (composto.indexOf(caractere) > -1) {

                        if (caractere == ':') {
                            digerir();
                            if (caractere == '=') {
                                tabelalex.push([linha, coluna - 1, ':=', ':=']);

                            } else {
                                console.log(`Erro 1, linha (${linha},${coluna}): Esperava um =`);
                                process.exit();

                            }
                        } else if (caractere == '<') {
                            digerir();

                            if (caractere == '>') {
                                tabelalex.push([linha, coluna - 1, '<>', '<>']);

                            } else if (caractere == '=') {
                                tabelalex.push([linha, coluna - 1, '<=', '<=']);

                            } else {
                                tabelalex.push([linha, coluna - 1, '<', '<']);
                                continue;

                            }
                        } else if (caractere == '>') {
                            digerir();

                            if (caractere == '=') {
                                tabelalex.push([linha, coluna - 1, '>=', '>=']);

                            } else {
                                tabelalex.push([linha, coluna - 1, '>', '>']);

                                continue;
                            }
                        }
                    } else if (letras.indexOf(caractere) > -1) {
                        var buffer = caractere;

                        while (identificadores.indexOf(digerir()) > -1 && caractere !== String.fromCharCode(0)) {
                            buffer += caractere;
                        }

                        if (palavrasReservadas.indexOf(buffer) > -1) {
                            tabelalex.push([linha, coluna - buffer.length, buffer, buffer]);

                        } else {
                            tabelalex.push([linha, coluna - buffer.length, 'ID', buffer]);
                        }
                        continue;
                    } else if (numeros.indexOf(caractere) > -1) {
                        var buffer = caractere;

                        while (numeros.indexOf(digerir()) > -1 && caractere !== String.fromCharCode(0)) {
                            buffer += caractere;
                        }
                        if (caractere == '.') {
                            buffer += '.';

                            while (numeros.indexOf(digerir()) > -1 && caractere !== String.fromCharCode(0)) {
                                buffer += caractere;
                            }

                            tabelalex.push([linha, coluna - buffer.length, 'REAL', buffer]);
                        } else {
                            tabelalex.push([linha, coluna - buffer.length, 'INTEGER', buffer]);

                        }
                        continue;
                    } else if (' \n'.indexOf(caractere) < 0 || ''.indexOf(caractere) < 0) {
                        if (caractere != ' ' && caractere != '\n' && caractere != false) {
                            console.log(`Erro 1, linha (${linha},${coluna}): Não é um token válido`);
                            process.exit();
                        }
                        // console.log(`Erro 1, linha (${linha},${coluna}): Não é um token válido`);
                    }
                    digerir();
                }

                tabelaLexica = 'Linha\tColuna\tTipo\tValor\n';
                tabelalex.map((valor) => tabelaLexica += valor[0] + '\t' + valor[1] + '\t' + valor[2] + '\t' + valor[3] + '\n');

                fs.appendFile('./novaTabelaLexica.txt', tabelaLexica, () => {
                    console.log('arquivo Criado');
                    leitor.close();
                });
            } else {
                console.log('Arquivo inexistente');
                lerArquivo();
            }
        })
    })
}

lerArquivo();